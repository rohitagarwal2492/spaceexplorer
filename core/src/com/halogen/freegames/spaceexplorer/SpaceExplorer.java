package com.halogen.freegames.spaceexplorer;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.halogen.freegames.spaceexplorer.dataLoaders.GameAssetManager;
import com.halogen.freegames.spaceexplorer.dataLoaders.GameData;
import com.halogen.freegames.spaceexplorer.dataLoaders.SoundManager;
import com.halogen.freegames.spaceexplorer.screens.SplashScreen;
import com.halogen.freegames.spaceexplorer.utils.WidgetScreenStack;
import com.halogen.freegames.spaceexplorer.widgets.PrivacyWindow;

import java.util.HashMap;

public class SpaceExplorer extends Game {
	/*
	Used to make sure that Singleton classes are initialized only once.
	Not using static vars to enforce singleton as static vars are sometimes not de-initialized on
	phones leading to all kinds of bugs
	 */
    private HashMap<String, Integer> instanceCount;

	//camera and viewports
	private OrthographicCamera cam;
	private Viewport viewPort;
	private ExtendViewport extendViewPort;

	//batches and renderer objects
    private SpriteBatch batch;

    //Data and Asset Loaders
    private GameData gameData;
    private GameAssetManager assetManager;
    private SoundManager soundManager;

    //Widget Stack
    private WidgetScreenStack widgetScreenStack;

    //Shows the GDPR screen for Europe if needed
    public void showGDPRConsentPageIfNeeded(){
        //switch to privacy screen if needed
        //todo: add condition to open privacy widget
        widgetScreenStack.push(new PrivacyWindow(this));
    }

    private void resetCountForSingletons(){
        instanceCount.put("GameAssetManager",0);
        instanceCount.put("SoundManager",0);
        instanceCount.put("GameData",0);
        instanceCount.put("WidgetScreenStack",0);
    }

	@Override
	public void create () {
        //initialize count for singletons
        instanceCount = new HashMap<String, Integer>();
        resetCountForSingletons();

	    //init renderer and batch
		batch = new SpriteBatch();

        //Set virtual world size
        final float Virtual_Width = 960;
        final float Virtual_Height = 540;

		//initialize camera and viewports
		cam = new OrthographicCamera(Virtual_Width, Virtual_Height);
		cam.setToOrtho(false, Virtual_Width, Virtual_Height);
		viewPort = new FitViewport(Virtual_Width, Virtual_Height, cam);
		extendViewPort = new ExtendViewport(Virtual_Width, Virtual_Height, cam);
		cam.position.set(viewPort.getWorldWidth()/2, viewPort.getWorldHeight()/2, 0);

		//init data and assets loaders (sound manager is loaded from withing game asset manager.
        gameData = new GameData(this);
        assetManager = new GameAssetManager(this);

        //init widget stack
        widgetScreenStack = new WidgetScreenStack(this);

        /*
        using "this" to pass this object's handle so that the screen
        can use spriteBatch, cam, assetManagers etc. We'll pass game handle to most of the classes
        */
        setScreen(new SplashScreen(this));
	}

	@Override
	public void render () {
        //Game class delegates to the current screen hence using super
        //this will call the render method of currently set screen
        super.render();

        //render the widgets in order
        widgetScreenStack.draw();
	}

	@Override
	public void dispose () {
        System.out.println("Dispose called from game class");

        //dispose the delegated screen
        super.dispose();

        //dispose batches
		batch.dispose();

		//dispose assets
		assetManager.dispose();
	}

	@Override
	public void resize(int width, int height) {
		viewPort.update(width,height);
		extendViewPort.update(width,height);

		//call respective screen's resize method
		super.resize(width,height);
	}

	public void quitGame(){
        dispose();
        //Fixme: Is this right/best way?
        Gdx.app.exit();
    }

	//Getters and Setters
	public OrthographicCamera getCam(){
		return cam;
	}

	public Viewport getViewPort() {
		return viewPort;
	}

	public ExtendViewport getExtendViewPort() {
		return extendViewPort;
	}

    public SpriteBatch getBatch() {
        return batch;
    }

    public GameData getGameData() {
        return gameData;
    }

    public GameAssetManager getAssetManager() {
        return assetManager;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    public void setSoundManager(SoundManager soundManager){
        this.soundManager = soundManager;
    }

    public HashMap<String, Integer> getInstanceCount() {
        return instanceCount;
    }

    public WidgetScreenStack getWidgetScreenStack() {
        return widgetScreenStack;
    }
}
