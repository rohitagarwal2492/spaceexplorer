package com.halogen.freegames.spaceexplorer.utils;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.halogen.freegames.spaceexplorer.SpaceExplorer;

/*
Defines the options widget group
 */

public class WidgetScreen extends WidgetGroup {

    private SpaceExplorer game;

    private ImageButton closeButton;

    private NinePatch bg;

    public WidgetScreen(SpaceExplorer game){
        this.game = game;

        this.setPosition(game.getViewPort().getWorldWidth()*0.1f, game.getViewPort().getWorldHeight()*0.1f);
        this.setSize(game.getViewPort().getWorldWidth()*0.8f,game.getViewPort().getWorldHeight()*0.8f);

        //create bg
        bg = new NinePatch(game.getAssetManager().widgetScrBG);

        //hardcoded because widgetScreenBG is a 9 point tex whose corner won't scale up based on scr size
        float buttSize = 25;
        closeButton = new ImageButton(game.getAssetManager().widgetScrCloseButtonDrawable);
        closeButton.setSize(buttSize,buttSize);
        float closeButtonOffset = game.getAssetManager().widgetScrCircCenterOffset + buttSize/2;
        closeButton.setPosition(this.getWidth() - closeButtonOffset, this.getHeight() - closeButtonOffset);
        addUIListener();

        this.addActor(closeButton);
    }

    private void addUIListener(){
        closeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getWidgetScreenStack().pop();
            }
        });
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        bg.draw(batch,this.getX(),this.getY(),this.getWidth(),this.getHeight());
        super.draw(batch, parentAlpha);
    }
}
