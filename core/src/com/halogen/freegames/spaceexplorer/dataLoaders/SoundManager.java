package com.halogen.freegames.spaceexplorer.dataLoaders;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.halogen.freegames.spaceexplorer.SpaceExplorer;

/**
 * Creates one instance of each sound,music and these are shared across all the app elements
 */

public class SoundManager {
    //Make this a singleton
    private static int INSTANCE_COUNT = 0;

    //handles
    private AssetManager manager;
    private Preferences prefs;

    //Music
    public Music mainMenuMusic;

    private float soundVolume;

    //used for sounds played once every move
    private boolean soundsIdle;

    SoundManager(SpaceExplorer game, AssetManager manager){
        //throw if instance already created
        if(game.getInstanceCount().get("SoundManager") != 0){
            throw new RuntimeException("Sound Manager should only be initialized once");
        }
        game.getInstanceCount().put("SoundManager",1);

        this.manager = manager;
        prefs = game.getGameData().getPreferences();

        soundVolume = prefs.getFloat("soundVolume", 1.0f);
    }

    public void loadAssets(){
        manager.load("music/memories.mp3",Music.class);
    }

    public void assignAssetsToVars(){
        float musicVolume = prefs.getFloat("musicVolume", 1.0f);

        mainMenuMusic = manager.get("music/memories.mp3",Music.class);
        mainMenuMusic.setVolume(musicVolume);
        mainMenuMusic.setLooping(true);
    }

    public void playButtonTap(){
        //todo: fill up
    }
}
