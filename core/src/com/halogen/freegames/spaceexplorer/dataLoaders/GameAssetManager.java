package com.halogen.freegames.spaceexplorer.dataLoaders;

/*
 * All styles and their assets are loaded here
 */

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.halogen.freegames.spaceexplorer.SpaceExplorer;

/*
    NOTE (Very Important):
    Do Not create UI elements here like Image button because
    if created and directly assigned to vars in screens, listeners
    will be multiply applied on every instantiation of screen.
    Only create assets like texture etc and instantiate buttons
    during screen instantiation
*/

public class GameAssetManager {
    //Game Handle
    private SpaceExplorer game;

    //asset manager allow loading assets asynchronously
    private AssetManager manager;

    //common assets
    //WidgetScr BGs
    public NinePatch widgetScrBG;
    public Drawable widgetScrCloseButtonDrawable;
    public float widgetScrCircCenterOffset;

    //Main Menu Assets
    public Texture mainMenuTitle;
    public Drawable mainMenuPlayButtonDrawable;
    public Drawable mainMenuOptionsButtonDrawable;
    public Drawable mainMenuHelpButtonDrawable;

    //Level Select Assets
    //fixme: these should be automatically generated as they will be variable in number
    public Drawable levelSelectPlayButtonDrawable;
    public Drawable levelSelectBackButtonDrawable;


    public GameAssetManager(SpaceExplorer game){
        //throw if instance already created
        if(game.getInstanceCount().get("GameAssetManager") != 0){
            throw new RuntimeException("GameAssetManager should only be initialized once");
        }
        game.getInstanceCount().put("GameAssetManager",1);

        this.game = game;
        manager = new AssetManager();
        game.setSoundManager(new SoundManager(this.game,manager));
        loadAssets();
    }

    public boolean isLoaded(){
        //manager.update() returns true once all assets are loaded otherwise it loads assets async and returns false
        if(manager.update()){
            assignAssetsToVars();
            return true;
        }
        return false;
    }

    //loads assets for different screens in memory
    private void loadAssets(){
        loadCommonAssets();
        loadMainMenuAssets();
        loadLevelSelectAssets();
        game.getSoundManager().loadAssets();
    }

    private void assignAssetsToVars(){
        assignCommonAssets();
        assignMainMenuAssets();
        assignLevelSelectAssets();
        game.getSoundManager().assignAssetsToVars();
    }

    private void loadCommonAssets(){
        manager.load("common/widgetScrBG.png", Texture.class);
        manager.load("common/widgetScrClose.png", Texture.class);
    }

    private void assignCommonAssets(){
        //this tex size is 303 (rect(256) + half extruding circle(32) + lineWidth(15)), put buttons accordingly
        Texture tex = manager.get("common/widgetScrBG.png", Texture.class);
        widgetScrBG = new NinePatch(tex,64,303-64-15,303-64-15,64);

        widgetScrCloseButtonDrawable = new TextureRegionDrawable(new TextureRegion(manager.get("common/widgetScrClose.png", Texture.class)));

        widgetScrCircCenterOffset = 7.5f + 32;
    }

    private void loadMainMenuAssets(){
        manager.load("titles/mainMenu.png", Texture.class);
        manager.load("buttons/play.png", Texture.class);
        manager.load("buttons/options.png", Texture.class);
        manager.load("buttons/help.png", Texture.class);
    }

    private void assignMainMenuAssets(){
        mainMenuTitle = manager.get("titles/mainMenu.png", Texture.class);

        mainMenuPlayButtonDrawable = new TextureRegionDrawable(new TextureRegion(manager.get("buttons/play.png", Texture.class)));
        mainMenuOptionsButtonDrawable = new TextureRegionDrawable(new TextureRegion(manager.get("buttons/options.png", Texture.class)));
        mainMenuHelpButtonDrawable = new TextureRegionDrawable(new TextureRegion(manager.get("buttons/help.png", Texture.class)));
    }

    private void loadLevelSelectAssets(){
        manager.load("buttons/level_select_play.png", Texture.class);
        manager.load("buttons/level_select_back.png", Texture.class);
    }

    private void assignLevelSelectAssets(){
        levelSelectPlayButtonDrawable = new TextureRegionDrawable(new TextureRegion(manager.get("buttons/level_select_play.png", Texture.class)));
        levelSelectBackButtonDrawable = new TextureRegionDrawable(new TextureRegion(manager.get("buttons/level_select_back.png", Texture.class)));
    }

    public void dispose(){
        System.out.println("Assets disposed");
        //below will dispose sound assets as well because it uses the same instance of asset manager
        manager.dispose();
    }

    //Setters and Getters
    public AssetManager getManager(){
        return manager;
    }
}
