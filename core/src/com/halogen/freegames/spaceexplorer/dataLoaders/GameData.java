package com.halogen.freegames.spaceexplorer.dataLoaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.halogen.freegames.spaceexplorer.SpaceExplorer;

/**
 * Created by Rohit on 13-08-2017.
 * All the members should ideally be static
 */

public class GameData {
    private SpaceExplorer game;

    //font sizes are hardcoded according to this base width and then scaled appropriately based on actual width of screen
    private float baseWidth;

    //GL Clear Color
    private Color clearColor;

    //preferences object is used to store data in memory
    private Preferences prefs;

    public GameData(SpaceExplorer game){
        //throw if instance already created
        if(game.getInstanceCount().get("GameData") != 0){
            throw new RuntimeException("GameData should only be initialized once");
        }
        game.getInstanceCount().put("GameData",1);

        this.game = game;
        baseWidth = 540;
        clearColor = new Color(1,1,1,1);

        //init prefs and player data
        prefs = Gdx.app.getPreferences("preferences");
    }

    //Getters and Setters
    public Preferences getPreferences(){
        return prefs;
    }

    public Color getClearColor() {
        return clearColor;
    }
}
