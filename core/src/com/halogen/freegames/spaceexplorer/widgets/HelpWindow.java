package com.halogen.freegames.spaceexplorer.widgets;

import com.halogen.freegames.spaceexplorer.SpaceExplorer;
import com.halogen.freegames.spaceexplorer.utils.WidgetScreen;

/*
Defines the options widget
 */

public class HelpWindow extends WidgetScreen {
    public HelpWindow(SpaceExplorer game){
        //need to call the super class constructor explicitly as it is parametrized
        super(game);
    }
}
