package com.halogen.freegames.spaceexplorer.widgets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.halogen.freegames.spaceexplorer.SpaceExplorer;
import com.halogen.freegames.spaceexplorer.utils.WidgetScreen;

/*
Defines the options widget
 */

public class OptionsWindow extends WidgetScreen {
    public OptionsWindow(SpaceExplorer game){
        //need to call the super class constructor explicitly as it is parametrized
        super(game);
    }
}
