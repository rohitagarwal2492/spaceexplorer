package com.halogen.freegames.spaceexplorer.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.halogen.freegames.spaceexplorer.SpaceExplorer;

/**
 * Created by Rohit on 22-10-2017.
 *
 * Defines the splash screen with company logo
 */

public class PlayScreen implements Screen {

    private SpaceExplorer game;

    PlayScreen(SpaceExplorer game){
        this.game = game;
    }

    @Override
    public void show() {

    }

    private void update(float dt){
        //todo: fill this
    }

    @Override
    public void render(float delta) {
        update(delta);

        Color clearColor = game.getGameData().getClearColor();
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.getBatch().setProjectionMatrix(game.getCam().combined);

        game.getBatch().begin();

        //todo: fill this

        game.getBatch().end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void hide() {}

    @Override
    public void dispose() {}
}
