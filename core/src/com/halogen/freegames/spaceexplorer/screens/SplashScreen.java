package com.halogen.freegames.spaceexplorer.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.halogen.freegames.spaceexplorer.SpaceExplorer;

/**
 * Created by Rohit on 22-10-2017.
 *
 * Defines the splash screen with company logo
 */

public class SplashScreen implements Screen {

    private SpaceExplorer game;
    private Texture logo;

    private float splashDuration;
    private float elapsed;

    private float initMargin;
    private float margin;
    private float targetMargin;
    private float logoWidth;
    private float logoHeight;

    public SplashScreen(SpaceExplorer game){
        this.game = game;

        //load assets
        logo = new Texture("common/com_logo.png");
        logo.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        splashDuration = 5;
        elapsed = 0;
    }

    @Override
    public void show() {

    }

    private void update(float dt){
        elapsed += dt;
        margin = initMargin + Interpolation.circleOut.apply(Math.min(elapsed/splashDuration,1.0f)) * (targetMargin - initMargin);

        logoWidth = game.getViewPort().getWorldWidth() - 2*margin;
        logoHeight = logoWidth*logo.getHeight()/logo.getWidth();

        if(game.getAssetManager().isLoaded() && elapsed > splashDuration){
            game.setScreen(new MainMenuScreen(game));
            //we need to ask for privacy consent for European region as per new GDPR rule
            //todo: remove from here and send to main menu screen
            //game.showGDPRConsentPageIfNeeded();
        }
    }

    @Override
    public void render(float delta) {
        update(delta);

        Color clearColor = game.getGameData().getClearColor();
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.getBatch().setProjectionMatrix(game.getCam().combined);

        game.getBatch().begin();
        game.getBatch().draw(logo,margin,game.getViewPort().getWorldHeight()/2 - logoHeight/2,logoWidth,logoHeight);
        game.getBatch().end();

        elapsed += delta;
    }

    @Override
    public void resize(int width, int height) {
        initMargin = game.getViewPort().getWorldWidth()/10;
        targetMargin = game.getViewPort().getWorldWidth()/6;
        logoWidth = game.getViewPort().getWorldWidth() - 2*initMargin;
        logoHeight = logoWidth*logo.getHeight()/logo.getWidth();
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void hide() {}

    @Override
    public void dispose() {}
}
