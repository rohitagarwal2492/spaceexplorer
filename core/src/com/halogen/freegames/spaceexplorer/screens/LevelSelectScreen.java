package com.halogen.freegames.spaceexplorer.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.halogen.freegames.spaceexplorer.SpaceExplorer;

/**
 * Created by Rohit on 01-08-2017.
 *
 * Opening screen for the game
 */

public class LevelSelectScreen implements Screen {

    //the game for sprite batch
    private SpaceExplorer game;

    //To add the buttons on the screen
    private Stage stage;

    private ImageButton playButton;
    private ImageButton backButton;

    //Constructor
    LevelSelectScreen(SpaceExplorer game){
        this.game = game;

        //start music
        game.getSoundManager().mainMenuMusic.play();

        //stage
        stage = new Stage(game.getViewPort(), game.getBatch());

        playButton = new ImageButton(game.getAssetManager().levelSelectPlayButtonDrawable);
        backButton = new ImageButton(game.getAssetManager().levelSelectBackButtonDrawable);
        addUIListeners();

        playButton.setSize(100,100);
        playButton.setPosition(game.getViewPort().getWorldWidth()/2 - playButton.getWidth()/2,game.getViewPort().getWorldHeight()/2 - playButton.getHeight()/2);

        backButton.setSize(100,100);
        backButton.setPosition(120,120);

        stage.addActor(playButton);
        stage.addActor(backButton);

        //to allow stage to identify events
        Gdx.input.setInputProcessor(stage);
    }

    private void addUIListeners(){
        playButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getSoundManager().playButtonTap();
                Gdx.input.setInputProcessor(null);
                dispose();
                openPlayScreen();
            }
        });

        backButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getSoundManager().playButtonTap();
                Gdx.input.setInputProcessor(null);
                dispose();
                openMainMenu();
            }
        });
        stage.addListener(new InputListener(){
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if(keycode == Input.Keys.BACK){
                    game.quitGame();
                    return true;
                }
                return false;
            }
        });
    }

    private void openMainMenu(){
        game.setScreen(new MainMenuScreen(game));
    }

    private void openPlayScreen(){
        game.setScreen(new PlayScreen(game));
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        stage.getRoot().setColor(1,1,1,1);
    }

    @Override
    public void render(float delta){
        stage.act(delta);

        Color clearColor = Color.GREEN;
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {}

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        System.out.println("Level Select Screen Disposed");
        stage.dispose();
    }
}
