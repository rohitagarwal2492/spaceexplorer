package com.halogen.freegames.spaceexplorer.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.halogen.freegames.spaceexplorer.SpaceExplorer;
import com.halogen.freegames.spaceexplorer.utils.WidgetScreen;
import com.halogen.freegames.spaceexplorer.widgets.HelpWindow;
import com.halogen.freegames.spaceexplorer.widgets.OptionsWindow;

/**
 * Created by Rohit on 01-08-2017.
 *
 * Opening screen for the game
 */

public class MainMenuScreen implements Screen {

    //the game for sprite batch
    private SpaceExplorer game;

    //To add the buttons on the screen
    private Stage stage;
    private boolean isPaused;

    private Texture title;

    private ImageButton playButton;
    private ImageButton helpButton;
    private ImageButton optionsButton;

    //Constructor
    MainMenuScreen(SpaceExplorer game){
        this.game = game;
        isPaused = false;

        //start music
        game.getSoundManager().mainMenuMusic.play();

        title = game.getAssetManager().mainMenuTitle;

        //stage
        stage = new Stage(game.getViewPort(), game.getBatch());

        playButton = new ImageButton(game.getAssetManager().mainMenuPlayButtonDrawable);
        optionsButton = new ImageButton(game.getAssetManager().mainMenuOptionsButtonDrawable);
        helpButton = new ImageButton(game.getAssetManager().mainMenuHelpButtonDrawable);
        addUIListeners();

        playButton.setSize(100,100);
        playButton.setPosition(game.getViewPort().getWorldWidth()/2 - playButton.getWidth()/2,game.getViewPort().getWorldHeight()/2 - playButton.getHeight()/2);

        optionsButton.setSize(100,100);
        optionsButton.setPosition(game.getViewPort().getWorldWidth()/2 - playButton.getWidth()/2 - 200,game.getViewPort().getWorldHeight()/2 - playButton.getHeight()/2 - 100);

        helpButton.setSize(100,100);
        helpButton.setPosition(game.getViewPort().getWorldWidth()/2 - playButton.getWidth()/2 + 200,game.getViewPort().getWorldHeight()/2 - playButton.getHeight()/2 - 100);

        stage.addActor(optionsButton);
        stage.addActor(helpButton);
        stage.addActor(playButton);

        //to allow stage to identify events
        Gdx.input.setInputProcessor(stage);
    }

    private void addUIListeners(){
        playButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getSoundManager().playButtonTap();
                openLevelSelectScreen();
            }
        });

        helpButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getSoundManager().playButtonTap();
                openHelp();
            }
        });

        optionsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getSoundManager().playButtonTap();
                openOptions();
            }
        });
        stage.addListener(new InputListener(){
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if(keycode == Input.Keys.BACK){
                    game.quitGame();
                    return true;
                }
                return false;
            }
        });
    }

    private void openLevelSelectScreen(){
        Gdx.input.setInputProcessor(null);
        dispose();
        game.setScreen(new LevelSelectScreen(game));
    }

    private void openHelp(){
        game.getWidgetScreenStack().push(new HelpWindow(this.game));
    }

    private void openOptions(){
        game.getWidgetScreenStack().push(new OptionsWindow(this.game));
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        stage.getRoot().setColor(1,1,1,1);
    }

    @Override
    public void render(float delta){
        Color clearColor = Color.PURPLE;
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.getBatch().setProjectionMatrix(game.getCam().combined);

        game.getBatch().begin();
        game.getBatch().draw(title,game.getViewPort().getWorldWidth()/2 - title.getWidth()/2,game.getViewPort().getWorldHeight() - title.getHeight() - 100);
        game.getBatch().end();

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
        isPaused = true;
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void resume() {
        isPaused = false;
        Gdx.input.setInputProcessor(stage);
    }

    public boolean isScreenPaused(){
        return isPaused;
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        System.out.println("Main Menu Screen Disposed");
        stage.dispose();
    }
}
